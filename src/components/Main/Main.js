import React, { Component } from 'react';
import axios from 'axios';
import { Form, Input, Button } from 'reactstrap';

//import './Main.css';

class Main extends Component {
  state = {
    // checkedURLs: [
    //   {effective_url: 'http://www.msn2.com', http_code: 200 },
    //   {effective_url: 'http://www.google2.com', http_code: 200 }
    // ]
  };

  formHandler = (event) => {
    event.preventDefault();

    let urls = event.target.urls.value.split(/\r?\n/);
    let checkedURLs = [];

    if (urls.length <= 1 && urls[0].trim() == '') {
      alert('Please input a URL');
      return;
    }

    urls.map(url => {
      axios.get(`http://localhost/checkurl.php?url=${url.trim()}`)
        .then(response => {
          Object.values(response).map((r) => {
            if (r.effective_url) {
              checkedURLs.push({
                effective_url: r.effective_url,
                http_code: r.http_code
              });
            }
          });

          this.setState({checkedURLs});
        })
        .catch(error => { console.log(error) });
    });
  }

  render() {
    let resultsView = null;

    if (this.state.checkedURLs !== undefined) {
      resultsView = this.state.checkedURLs.map(r => <div>{r.effective_url} - {r.http_code}</div>);
    }

    /* return (
      <div>
        <h2>Enter the URL(s) to be checked, separate it by new line.</h2>
        <div>
          <form className="formStyle" onSubmit={this.formHandler}>
            <div><textarea name="urls" className="textarea"></textarea></div>
            <div><button className="button-check">Check</button></div>
          </form>
        </div>
        <div>
          Resultview:
          {resultsView}
        </div>
      </div>
    ); */

    return (
      <div>
        <h2>Enter the URL(s) to be checked, separate it by new line.</h2>
        <div>
          <Form onSubmit={this.formHandler}>
            <div><Input type="textarea" className="form-control col-lg-4 col-lg-offset-4 mx-auto mt-4 mb-5" name="urls" rows="8"></Input></div>
            <div><Button color="primary">Check</Button></div>
          </Form>
        </div>
        <div>
          Resultview:
          {resultsView}
        </div>
      </div>
    );
  };
}

export default Main;