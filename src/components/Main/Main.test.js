import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Main from './Main';

configure({adapter: new Adapter()});

console.log('DELDEBUG');

describe('<Main />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Main />);
    });

    test('should have a <form> element', () => {
        //console.log(wrapper.find('form').debug());

        expect(wrapper.find('form')).toHaveLength(1);
        expect(wrapper.find('.formStyle')).toHaveLength(1);
    });

    describe('when checking URLs', () => {
      let URLs = `http://delgreg.rf.gd
        http://www.google.com`;

      // simulates typing URLS then clicking the "Check" button
      beforeEach(() => {
        wrapper.find('textarea').simulate('change', {
          target: { value: URLs }
        });
        // wrapper.find('.button-check').simulate('click');
      });

      it('outputs result(s)', () => {
        console.log(wrapper.find('form').debug());
        expect(wrapper.find('.result').exists()).toBe(true);
      });
    });
});